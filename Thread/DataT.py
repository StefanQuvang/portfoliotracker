import threading
import time
from Data import Data


class DataThread (threading.Thread):

    def __init__(self, thread_id, name):
        threading.Thread.__init__(self)
        self.clData = Data()
        self.ThreadId = thread_id
        self.Name = name
        self.Stop = False

    def _print(self, arg):
        try:
            print(self.Name + " " + str(self.ThreadId) + " " + str(arg))
        except TypeError:
            print(self.Name + " " + str(self.ThreadId) + " " + arg)

    def stop(self, bool):
        self.Stop = bool

    def run(self):
        self._print("Starting")
        while not self.Stop:
            time.sleep(self.clData.Rate)
            for stock_list in self.clData.AddStocks:
                stock = self.clData.update_stock(stock_list)
            self._print("Stock updated")
        self._print("Exiting")
