import threading
import time
from Data import Data


class GuiThread (threading.Thread):

    def __init__(self, thread_id, name, gui):
        threading.Thread.__init__(self)
        self.clGUI = gui
        self.clData = Data()
        self.ThreadId = thread_id
        self.Name = name
        self.Stop = False

    def _print(self, arg):
        try:
            print(self.Name + " " + str(self.ThreadId) + " " + str(arg))
        except TypeError:
            print(self.Name + " " + str(self.ThreadId) + " " + arg)

    def stop(self, bool):
        self.Stop = bool

    def run(self):
        self._print("Starting")
        while not self.Stop:
            time.sleep(60)
            self.clGUI.update_stock_frame()
            self.clData.add_total_value()
            self.clGUI.add_graph()
            self._print("Frame updated")
        self._print("Exiting")