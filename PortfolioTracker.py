# Import classes from your brand new package
from GUI import GUI
from datetime import date
from Thread.DataT import DataThread
from Thread.GuiT import GuiThread


t_data = DataThread(1, "Signal Processing")
t_data.start()


today = date.today()

basic = ["Name", "Stefan", "Time", today]

B = GUI(basic=basic)

t_gui = GuiThread(2, "Gui Update", B)
t_gui.start()

B.start_box()

t_data.stop(True)
del t_data

t_gui.stop(True)
del t_gui
