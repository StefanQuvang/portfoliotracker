import tkinter as tk


def stock_overview(self):
    value_row = ["Total", "Treynor Measure", 0.9, "Sharpe Ratio", 0.9, "Jensen Measure", 0.9, "Dividend", 0.9, "Return", 0.9, "Dividend/Return", 0.9]
    row = 0
    col = 0
    for i in range(0, len(value_row)):
        label = tk.Text(master=self.StockOFrame, height=2, width=15)
        label.configure(bg=self.Black, bd=0, fg=self.White)
        label.grid(row=row, column=col)
        label.tag_configure("center", justify='center')
        label.insert(tk.END, value_row[i])
        label.tag_add("center", "1.0", "end")

        if col == 2:
            col = 1
            row = row + 1
        elif i == 0:
            col = 1
            row = 1
        else:
            col = col + 1


def highlight(self, arg):
    self.NewStock.configure(bg=self.grey)
    self.label.configure(bg=self.grey)