import tkinter as tk


def cb_add_btn(self):
    stock = []
    stock.append(self.NewStockEntry.get())
    stock.append(self.NewNumberEntry.get())
    stock.append(self.NewPriceEntry.get())
    stock.append(self.BuyDateEntry.get())
    stock_row = self.clData.add_stock(stock=stock)
    if stock_row:
        self.add_new_stock(stock_row=stock_row)
        self.add_graph()
        self.update_total()
        self.stock_win.destroy()


def add_new_stock(self, stock_row):
    self.NewStock = tk.Frame(self.StockFrame, height=100, width=400, bg=self.Black)
    self.NewStock.grid(row=self.Row, column=0)
    self.NewStock.grid_propagate(flag=False)
    row = 0
    column = 0
    color = "white"
    for i in range(0, len(stock_row)):
        if i == 1:
            column = 3
            color = "white"
            stock_row[i] = round(float(stock_row[i]), 2)
        elif i == 2:
            row = 1
            column = 1
            color = "white"
        elif i == 5:
            row = 2
            column = 1
            color = "white"
        elif i > 1 and self.is_digit(stock_row[i]):
            stock_row[i] = round(float(stock_row[i]), 2)
            if float(stock_row[i]) > 0:
                color = "green"
            else:
                color = "red"
            if i == 3 or i == 6:
                stock_row[i] = str(stock_row[i]) + "%"
        self.label = tk.Text(self.NewStock, height=self.Height, width=12)
        self.label.configure(bg=self.Black, fg=color, bd=0)
        self.label.grid(row=row, column=column)
        self.label.tag_configure("center", justify='center')
        self.label.insert(tk.END, stock_row[i])
        self.label.tag_add("center", "1.0", "end")
        column = column + 1
    self.Row = self.Row + 1


def cb_stock_window(self):
    self.stock_win = tk.Toplevel(self.Master)
    self.stock_win.title("New Stock")
    self.stock_win.configure(bg=self.Grey)

    add_btn = tk.Button(self.stock_win, text="Add", command=self.cb_add_btn)
    add_btn.grid(row=1, column=2)
    add_btn.configure(bg=self.Grey)

    self.NewStockEntry = tk.Entry(self.stock_win, bd=5, width=20)
    self.NewStockEntry.grid(row=0, column=1)
    self.NewStockEntry.configure(bg=self.Grey)

    label = tk.Text(self.stock_win, height=1, width=10)
    label.configure(bg=self.Grey, bd=0)
    label.grid(row=0, column=0)
    label.insert(tk.END, "Ticker")

    self.NewNumberEntry = tk.Entry(self.stock_win, bd=5, width=20)
    self.NewNumberEntry.grid(row=1, column=1)
    self.NewNumberEntry.configure(bg=self.Grey)

    label = tk.Text(self.stock_win, height=1, width=10)
    label.configure(bg=self.Grey, bd=0)
    label.grid(row=1, column=0)
    label.insert(tk.END, "Number")

    self.NewPriceEntry = tk.Entry(self.stock_win, bd=5, width=20)
    self.NewPriceEntry.grid(row=2, column=1)
    self.NewPriceEntry.configure(bg=self.Grey)

    label = tk.Text(self.stock_win, height=1, width=10)
    label.configure(bg=self.Grey, bd=0)
    label.grid(row=2, column=0)
    label.insert(tk.END, "Price")

    self.BuyDateEntry = tk.Entry(self.stock_win, bd=5, width=20)
    self.BuyDateEntry.grid(row=3, column=1)
    self.BuyDateEntry.configure(bg=self.Grey)

    label = tk.Text(self.stock_win, height=1, width=10)
    label.configure(bg=self.Grey, bd=0)
    label.grid(row=3, column=0)
    label.insert(tk.END, "Buy Date")

    label = tk.Text(self.stock_win, height=1, width=15)
    label.configure(bg=self.Grey, bd=0)
    label.grid(row=3, column=2)
    label.insert(tk.END, "YYYY-MM-DD")


def cb_remove_btn(self):
    ticker = self.RemoveStockEntry.get()
    self.clData.remove_stock(ticker)
    self.update_stock_frame()
    self.add_graph()
    self.remove_win.destroy()


def cb_delete_window(self):
    self.remove_win = tk.Toplevel(self.Master)
    self.remove_win.title("Remove Stock")
    self.remove_win.configure(bg=self.Grey)

    remove_btn = tk.Button(self.remove_win, text="Remove", command=self.cb_remove_btn)
    remove_btn.grid(row=0, column=2)
    remove_btn.configure(bg=self.Grey)

    self.RemoveStockEntry = tk.Entry(self.remove_win, bd=5, width=30)
    self.RemoveStockEntry.grid(row=0, column=1)
    self.RemoveStockEntry.configure(bg=self.Grey)


def update_stock_frame(self):
    for widget in self.StockFrame.winfo_children():
        widget.destroy()
    self.update_total()
    self.break_line()
    for i in self.clData.get_stock():
        self.add_new_stock(i)


def update_total(self):
    stock_row = self.clData.get_total()
    self.TotalStock = tk.Frame(self.StockFrame, height=100, width=400, bg=self.Black)
    self.TotalStock.grid(row=0, column=0)
    self.TotalStock.grid_propagate(flag=False)

    row = 0
    column = 0
    color = "white"
    for i in range(0, len(stock_row)):
        if i == 1:
            column = 3
            color = "white"
        elif i == 2:
            row = 1
            column = 1
            color = "white"
        elif i == 5:
            row = 2
            column = 1
            color = "white"
        elif i > 1 and self.is_digit(stock_row[i]):
            if float(stock_row[i]) > 0:
                color = "green"
            else:
                color = "red"
            if i == 3 or i == 6:
                stock_row[i] = str(stock_row[i]) + "%"
        self.label = tk.Text(self.TotalStock, height=self.Height, width=12)
        self.label.configure(bg=self.Black, fg=color, bd=0)
        self.label.grid(row=row, column=column)
        self.label.tag_configure("center", justify='center')
        self.label.insert(tk.END, stock_row[i])
        self.label.tag_add("center", "1.0", "end")
        column = column + 1
    self.Row = self.Row + 1
