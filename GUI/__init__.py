class GUI:
    from ._SelectStock import stock_overview
    from ._Stock import cb_stock_window, cb_add_btn, add_new_stock, update_total, cb_delete_window,\
                        cb_remove_btn, update_stock_frame
    from ._Layout import break_line, layout, add_button, add_label, add_graph, init, get_color, update_rate
    from ._Menu import menu, new_file, open_file, y_finance, about

    def __init__(self, basic=[]):
        ''' Constructor for this class. '''
        self.Grey = "#9C9C9C"
        self.Black = "black"
        self.LightGrey = "lightgrey"
        self.White = "white"
        self.Column = 0
        self.Row = 0
        self.Height = 2
        self.Width = 10
        self.Basic = basic

    def start_box(self):
        self.init()
        self.layout()
        self.add_button()
        self.add_label()
        self.menu()
        self.update_total()
        self.break_line()
        self.stock_overview()
        for i in self.clData.get_stock():
            self.add_new_stock(i)
        self.update_total()
        self.add_graph()

        self.Master.mainloop()

    def is_digit(self, n):
        try:
            float(n)
            return True
        except ValueError:
            return False
        except TypeError:
            return False
