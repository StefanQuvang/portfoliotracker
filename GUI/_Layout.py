import tkinter as tk
from Data import Data


def init(self):
    self.clData = Data()
    self.Master = tk.Tk()
    self.Master.configure(background=self.Black)
    self.Master.title("Portfolio Tracker")
    self.Master.grid_rowconfigure((0, 2), weight=1)
    self.Master.grid_columnconfigure((0, 2), weight=1)


def layout(self):
    self.LeftFrame = tk.Frame(self.Master, height=800, width=400)
    self.LeftFrame.grid(row=0, column=0)
    self.LeftFrame.grid_propagate(flag=False)
    self.RightFrame = tk.Frame(self.Master, height=800, width=800)
    self.RightFrame.grid(row=0, column=1)
    self.RightFrame.grid_propagate(flag=False)

    self.MainFrame = tk.Frame(self.LeftFrame, height=100, width=400)
    self.MainFrame.configure(background=self.Grey)
    self.MainFrame.grid(row=0, column=0)
    self.MainFrame.grid_propagate(flag=False)

    self.StockFrame = tk.Frame(self.LeftFrame, height=700, width=400)
    self.StockFrame.configure(background=self.Black)
    self.StockFrame.grid(row=1, column=0)
    self.StockFrame.grid_propagate(flag=False)

    self.OverviewFrame = tk.Frame(self.RightFrame, height=275, width=800)
    self.OverviewFrame.config(background=self.Black)
    self.OverviewFrame.grid(row=0, column=0)
    self.OverviewFrame.grid_propagate(flag=False)

    self.StockOFrame = tk.Frame(self.OverviewFrame, height=275, width=400)
    self.StockOFrame.config(background=self.Black)
    self.StockOFrame.grid(row=0, column=0)
    self.StockOFrame.grid_propagate(flag=False)

    self.ChartFrame = tk.Frame(self.OverviewFrame, height=275, width=400)
    self.ChartFrame.config(background=self.Black)
    self.ChartFrame.grid(row=0, column=1)
    self.ChartFrame.grid_propagate(flag=False)

    self.MenuFrame = tk.Frame(self.RightFrame, height=50, width=800)
    self.MenuFrame.config(background=self.Grey)
    self.MenuFrame.grid(row=1, column=0)
    self.MenuFrame.grid_propagate(flag=False)

    self.GraphBtnFrame = tk.Frame(self.RightFrame, height=25, width=800, bg=self.Black)
    self.GraphBtnFrame.grid(row=2, column=0)
    self.GraphBtnFrame.grid_propagate(flag=False)

    self.GraphFrame = tk.Frame(self.RightFrame, height=450, width=800)
    self.GraphFrame.config(background=self.Black)
    self.GraphFrame.grid(row=3, column=0)
    self.GraphFrame.grid_propagate(flag=False)


def add_button(self):
    height = 3
    width = 6

    self.Historybtn = tk.Button(self.MenuFrame, text="History", height=height, width=width)
    self.Historybtn.configure(bg=self.Grey)
    self.Historybtn.grid(row=0, column=0)

    self.Btn1 = tk.Button(self.MenuFrame, text="Btn1", height=height, width=width)
    self.Btn1.configure(bg=self.Grey)
    self.Btn1.grid(row=0, column=1)

    self.Btn2 = tk.Button(self.MenuFrame, text="Btn2", height=height, width=width)
    self.Btn2.configure(bg=self.Grey)
    self.Btn2.grid(row=0, column=2)

    self.Btn3 = tk.Button(self.MenuFrame, text="Btn3", height=height, width=width)
    self.Btn3.configure(bg=self.Grey)
    self.Btn3.grid(row=0, column=3)

    photo_add = tk.PhotoImage(file=r"GUI/icon/add.png")
    photo_add = photo_add.subsample(1, 1)
    self.NewStockbtn = tk.Button(self.MainFrame, image=photo_add, command=self.cb_stock_window)
    self.NewStockbtn.image = photo_add
    self.NewStockbtn.configure(bg=self.Grey, bd=0)
    self.NewStockbtn.grid(row=0, column=1)

    photo_remove = tk.PhotoImage(file=r"GUI/icon/minus.png")
    photo_remove = photo_remove.subsample(1, 1)
    self.RemoveStockbtn = tk.Button(self.MainFrame,image=photo_remove, command=self.cb_delete_window)
    self.RemoveStockbtn.image = photo_remove
    self.RemoveStockbtn.configure(bg=self.Grey, bd=0)
    self.RemoveStockbtn.grid(row=0, column=2)

    width = 20
    one_day = tk.Button(master=self.GraphBtnFrame, text="1 Day")
    one_day.configure(bg=self.LightGrey, bd=0, width=width, fg=self.White)
    one_day.grid(row=0, column=0)

    one_week = tk.Button(master=self.GraphBtnFrame, text="1 Week")
    one_week.configure(bg=self.Black, bd=0, width=width, fg=self.White)
    one_week.grid(row=0, column=1)

    one_month = tk.Button(master=self.GraphBtnFrame, text="1 Month")
    one_month.configure(bg=self.Black, bd=0, width=width, fg=self.White)
    one_month.grid(row=0, column=2)

    one_year = tk.Button(master=self.GraphBtnFrame, text="1 Year")
    one_year.configure(bg=self.Black, bd=0, width=width, fg=self.White)
    one_year.grid(row=0, column=3)

    this_year = tk.Button(master=self.GraphBtnFrame, text="This Year")
    this_year.configure(bg=self.Black, bd=0, width=width, fg=self.White)
    this_year.grid(row=0, column=4)


def add_label(self):
    self.PortfolioLabel = tk.Text(self.MainFrame, height=10, width=40)
    self.PortfolioLabel.configure(bg=self.Grey, bd=0)
    self.PortfolioLabel.grid(row=0, column=3)
    self.PortfolioLabel.tag_configure("center", justify='center')
    self.PortfolioLabel.insert(tk.END, "Portfolio")
    self.PortfolioLabel.tag_add("center", "1.0", "end")


def break_line(self):
    break_line_frame = tk.Frame(self.StockFrame, height=25, width=400, bg=self.Grey)
    break_line_frame.grid(row=1, column=0)
    break_line_frame.grid_propagate(flag=False)

    width = 13
    height = 1

    one_sec_btn = tk.Button(break_line_frame, text="One Second", height=height, width=width)
    one_sec_btn.configure(bg=self.get_color("sec"), bd=0, command=lambda: self.update_rate("sec"))
    one_sec_btn.grid(row=0, column=0)

    one_min_btn = tk.Button(break_line_frame, text="One Minute", height=height, width=width)
    one_min_btn.configure(bg=self.get_color("min"), bd=0, command=lambda: self.update_rate("min"))
    one_min_btn.grid(row=0, column=1)

    one_hour_btn = tk.Button(break_line_frame, text="One Hour", height=height, width=width)
    one_hour_btn.configure(bg=self.get_color("hour"), bd=0, command=lambda: self.update_rate("hour"))
    one_hour_btn.grid(row=0, column=2)

    one_day_btn = tk.Button(break_line_frame, text="One Day", height=height, width=width)
    one_day_btn.configure(bg=self.get_color("day"), bd=0, command=lambda: self.update_rate("day"))
    one_day_btn.grid(row=0, column=3)

    self.Row = self.Row + 1


def update_rate(self, rate):
    self.clData.update_rate(rate)
    self.break_line()


def get_color(self, rate):
    rtn = self.Grey
    if rate == self.clData.return_rate():
        rtn = self.LightGrey
    return rtn


def add_graph(self):
    chart_canvas = self.clData.add_chart(frame=self.ChartFrame)
    chart_canvas.grid(row=0, column=0)
    chart_canvas = self.clData.add_graph(self.GraphFrame)
    chart_canvas.grid(row=1, column=0)
