from tkinter.filedialog import askopenfilename
import tkinter as tk


def menu(self):
    menu_object = tk.Menu(self.Master)
    self.Master.config(menu=menu_object)
    filemenu = tk.Menu(menu_object)
    menu_object.add_cascade(label="File", menu=filemenu)
    filemenu.add_command(label="New", command=self.new_file)
    filemenu.add_command(label="Open...", command=self.open_file)
    filemenu.add_separator()
    filemenu.add_command(label="Exit", command=self.Master.quit)

    stock_menu = tk.Menu(filemenu)
    stock_menu.add_cascade(label="Stock API", menu=stock_menu)
    stock_menu.add_command(label="Yahoo Finance API", command=self.y_finance)

    help_menu = tk.Menu(filemenu)
    menu_object.add_cascade(label="Help", menu=help_menu)
    help_menu.add_command(label="About...", command=self.about)


def y_finance(self):
    self.StockWindow = tk.Toplevel(self.Master)
    self.StockWindow.title("Stock Api")
    key_btn = tk.Button(self.StockWindow, text="Use API", command=self.AddKey)
    key_btn.grid(row=self.Row, column=0)
    self.API = "YahooFinance"
    self.Key = "None"
    self.Webpage = "https://finance.yahoo.com/"


def add_key(self):
    extra = ["API", self.API, "Key", self.Key, "Webpage", self.Webpage]
    self.clStock.set_api(self.API)
    self.clFile.add_to_basic(Extra=extra)
    self.clStock.credentials(self.Key)
    self.StockWindow.destroy()


def new_file(self):
    print("New File!")


def open_file(self):
    name = askopenfilename()
    print(name)


def about(self):
    print("This is a simple example of a menu")