import yfinance as yf
from datetime import datetime, timedelta
import pandas


class YahooFinance:
    def __init__(self):
        self.Stock = ""
        self.Ticker = None
        self.count = 0
        self.test = {datetime.now().strftime('%Y-%m-%d %H:%M:%S'): 0}

    def get_ticker(self):
        self.Ticker = yf.Ticker(self.Stock)

    def get_history(self, stock, period):
        if self.Stock != stock[0]:
            self.Stock = stock[0]
            self.get_ticker()
        print(stock[3])
        if stock[3] == (datetime.now() - timedelta(days=1)).strftime('%Y-%m-%d %H:%M:%S'):
            return[0, stock[3]]
        if datetime.today() > datetime.strptime(stock[3], '%Y-%m-%d') + timedelta(days=30):
            start = datetime.today() - timedelta(days=29)
        else:
            start = datetime.strptime(stock[3], '%Y-%m-%d')
        hist = self.Ticker.history( start=start.date(),
                                    end=(start + timedelta(days=1)).date(),
                                    interval="1m")
        tmp = []
        count = 0
        for i in hist.index.values:
            time = pandas.to_datetime(i).strftime('%Y-%m-%d %H:%M:%S')
            if time in self.test:
                self.test[time] = self.test[time] + hist.values.tolist()[count][3]*int(stock[1])
            else:
                self.test[time] =hist.values.tolist()[count][3]*int(stock[1])
            count = count + 1
        return [tmp, (start + timedelta(days=1)).date().strftime('%Y-%m-%d')]

    def get_info(self, tag_name, stock):
        if self.Stock != stock:
            self.Stock = stock
            self.get_ticker()
        if tag_name == "exDividendDate":
            return datetime.datetime.fromtimestamp(self.Ticker.info[tag_name])
        return self.Ticker.info[tag_name]

    def return_row(self):
        rtn = ['previousClose', 'ask']
        return rtn



#{'zip': '1092', 'sector': 'Financial Services', 'fullTimeEmployees': 22582, 'longBusinessSummary': 'Danske Bank A/S provides various banking products and services to small and medium-sized businesses; and corporate, institutional, and personal customers. It operates through Banking DK, Banking Nordic, Corporates and Institutions, Wealth Management, and Northern Ireland segments. The company offers daily banking, home financing, investment, and retirement planning solutions; strategic advisory services; leasing solutions; and financing, financial market, general banking, and corporate finance advisory services, as well as financial solutions and products in the areas of capital markets, fixed income and currencies, and transaction banking and investor services. It also provides wealth and asset management, pension savings, and insurance solutions covering life, health, and accident; and mobile banking services. In addition, the company provides mortgage finance and real-estate brokerage, as well as trades in fixed income products, foreign exchange, and equities. It has operations in Denmark, Finland, Sweden, Norway, the United Kingdom, Ireland, Estonia, Latvia, Lithuania, Luxembourg, Russia, Germany, Poland, the United States, India, and China. Danske Bank A/S was founded in 1871 and is headquartered in Copenhagen, Denmark.', 'city': 'Copenhagen', 'phone': '45 33 44 00 00', 'country': 'Denmark', 'companyOfficers': [], 'website': 'http://www.danskebank.com', 'maxAge': 1, 'address1': 'Holmens Kanal 2-12', 'industry': 'Banks—Regional', 'previousClose': 104.15, 'regularMarketOpen': 104.15, 'twoHundredDayAverage': 91.26276, 'trailingAnnualDividendYield': 0.08161306, 'payoutRatio': 0, 'volume24Hr': None, 'regularMarketDayHigh': 105.7, 'navPrice': None, 'averageDailyVolume10Day': 4182422, 'totalAssets': None, 'regularMarketPreviousClose': 104.15, 'fiftyDayAverage': 90.91194, 'trailingAnnualDividendRate': 8.5, 'open': 104.15, 'toCurrency': None, 'averageVolume10days': 4182422, 'expireDate': None, 'yield': None, 'algorithm': None, 'dividendRate': None, 'exDividendDate': 1584489600, 'beta': 0.977848, 'circulatingSupply': None, 'startDate': None, 'regularMarketDayLow': 103.55, 'priceHint': 2, 'currency': 'DKK', 'trailingPE': 11.895825, 'regularMarketVolume': 3408697, 'lastMarket': None, 'maxSupply': None, 'openInterest': None, 'marketCap': 89186033664, 'volumeAllCurrencies': None, 'strikePrice': None, 'averageVolume': 2878769, 'priceToSalesTrailing12Months': 2.2240906, 'dayLow': 103.55, 'ask': 104.6, 'ytdReturn': None, 'askSize': 0, 'volume': 3408697, 'fiftyTwoWeekHigh': 123.6, 'forwardPE': 5.2431073, 'fromCurrency': None, 'fiveYearAvgDividendYield': None, 'fiftyTwoWeekLow': 68.04, 'bid': 0, 'tradeable': False, 'dividendYield': None, 'bidSize': 0, 'dayHigh': 105.7, 'exchange': 'CPH', 'shortName': 'Danske Bank A/S', 'longName': 'Danske Bank A/S', 'exchangeTimezoneName': 'Europe/Copenhagen', 'exchangeTimezoneShortName': 'CET', 'isEsgPopulated': False, 'gmtOffSetMilliseconds': '3600000', 'quoteType': 'EQUITY', 'symbol': 'DANSKE.CO', 'messageBoardId': 'finmb_558679', 'market': 'dk_market', 'annualHoldingsTurnover': None, 'enterpriseToRevenue': 18.057, 'beta3Year': None, 'profitMargins': 0.20399, 'enterpriseToEbitda': None, '52WeekChange': 0.14299822, 'morningStarRiskRating': None, 'forwardEps': 19.95, 'revenueQuarterlyGrowth': None, 'sharesOutstanding': 852638976, 'fundInceptionDate': None, 'annualReportExpenseRatio': None, 'bookValue': 184.761, 'sharesShort': None, 'sharesPercentSharesOut': None, 'fundFamily': None, 'lastFiscalYearEnd': 1577750400, 'heldPercentInstitutions': 0.16317, 'netIncomeToCommon': 7548000256, 'trailingEps': 8.793, 'lastDividendValue': 8.5, 'SandP52WeekChange': 0.16240406, 'priceToBook': 0.5661368, 'heldPercentInsiders': 0.205, 'nextFiscalYearEnd': 1640908800, 'mostRecentQuarter': 1601424000, 'shortRatio': None, 'sharesShortPreviousMonthDate': None, 'floatShares': 670532255, 'enterpriseValue': 724078362624, 'threeYearAverageReturn': None, 'lastSplitDate': None, 'lastSplitFactor': None, 'legalType': None, 'lastDividendDate': 1584489600, 'morningStarOverallRating': None, 'earningsQuarterlyGrowth': -0.302, 'dateShortInterest': None, 'pegRatio': None, 'lastCapGain': None, 'shortPercentOfFloat': None, 'sharesShortPriorMonth': None, 'category': None, 'fiveYearAverageReturn': None, 'regularMarketPrice': 104.15, 'logo_url': 'https://logo.clearbit.com/danskebank.com'}
