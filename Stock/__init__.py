class Stock:
    from ._Set import credentials, set_api
    from ._Get import get_stock_info

    def __init__(self):
        # As Default use Yahoo Finance as API
        # This will be cleared if another is chosen in the GUI
        self.clAPI = None
        self.set_api("")
