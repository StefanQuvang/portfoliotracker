def get_stock_info(self, stock):
    rtn = [stock[0]]
    tmp = []
    for i in self.clAPI.return_row():
        tmp.append(self.clAPI.get_info(tag_name=i, stock=stock[0]))
    rtn.append(int(stock[1]) * tmp[1])
    rtn.append("Daily Gain")
    rtn.append((tmp[1] / tmp[0] - 1) * 100)
    rtn.append((tmp[1] - tmp[0]) * int(stock[1]))
    rtn.append("Overall Gain")
    rtn.append((tmp[1] / float(stock[2]) - 1) * 100)
    rtn.append((tmp[1] - float(stock[2])) * int(stock[1]))
    rtn.append(int(stock[1]))
    rtn.append(float(stock[2]))
    data = self.clAPI.get_history(stock, "1D")
    rtn.append(data[1])
    return [rtn, data[0]]
