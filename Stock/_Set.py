from YahooFinance import YahooFinance


def credentials(self, key):
    self.key = key


def set_api(self, api):
    del self.clAPI
    # This if statement does not make much sense, atm.
    # When new API is implemented the if will change to that API
    # And YahooFinance Is catched in the default.
    if api == "YahooFinance":
        self.ApiText = "YahooFinance"
        self.clAPI = YahooFinance()
    else:
        # No Valid option is chosen, to use the default
        # Which is the YahooFinance
        self.ApiText = "YahooFinance"
        self.clAPI = YahooFinance()
