import xml.etree.cElementTree as elem_tree


def write_total(self,standard_row, total):
    count = 0
    total_stock = self.x_stock.find("Total")
    if not total_stock:
        total_stock = elem_tree.SubElement(self.x_stock, "Total")
        for i in total:
            elem_tree.SubElement(total_stock, str(standard_row[count])).text = str(i)
            count = count + 1
    else:
        for i in total_stock.iter():
            if not i.text:
                i.text = str(total[count])
                count = count + 1
            elif i.text.strip():
                i.text = str(total[count])
                count = count + 1
    self.x_tree.write("Stocks.xml")


def write_to_file(self, name, content):
    count = 0
    stock = elem_tree.SubElement(self.x_stock, "Stock")
    for i in content:
        elem_tree.SubElement(stock, str(name[count])).text = str(i)
        count = count + 1
    self.x_tree.write("Stocks.xml")


def remove_from_file(self, index):
    iterator = list(self.x_stock.iter('Stock'))
    for item in iterator:
        for tag in item.itertext():
            if tag == index:
                self.x_stock.remove(item)
    self.x_tree.write("Stocks.xml")


def construct_file(self):
    self.x_root = elem_tree.Element("root")
    self.x_tree = elem_tree.ElementTree(self.x_root)
    self.x_doc = elem_tree.SubElement(self.x_root, "doc")
    self.basic()
    self.x_stock = elem_tree.SubElement(self.x_doc, "Stocks")
    self.x_total = elem_tree.SubElement(self.x_stock, "Total")
