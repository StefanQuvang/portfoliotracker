import xml.etree.cElementTree as elem_tree


def basic(self):
    stock = elem_tree.SubElement(self.x_doc,"Information")
    count = 0
    while count+2 <= len(self.BasicInfo):
        elem_tree.SubElement(stock, self.BasicInfo[count]).text = str(self.BasicInfo[count+1])
        count = count + 2


def add_to_basic(self, extra):
    self.x_info = self.x_doc.find("Information")
    count = 0
    api_elem = elem_tree.SubElement(self.x_info, "API")
    while count+2 <= len(extra):
        elem_tree.SubElement(api_elem, extra[count]).text = str(extra[count+1])
        count = count + 2
    self.xtree.write("Stocks.xml")
