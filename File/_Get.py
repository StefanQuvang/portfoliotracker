import xml.etree.cElementTree as ET
import os.path
import re
import csv
from pandas import DataFrame, Series


def read_from_file(self):
    stock = []
    if os.path.isfile('./Stocks.xml'):
        self.File = True
        self.x_tree = ET.parse('./Stocks.xml')
        self.x_root = self.x_tree.getroot()
        self.x_doc = self.x_root.find("doc")
        self.x_stock = self.x_doc.find("Stocks")
        for j in self.x_doc.find("Stocks").iter(tag="Stock"):
            tmp = []
            for i in j.itertext():
                if len(i.strip()) != 0:
                    tmp.append(i)
            stock.append(tmp)
    return stock


def read_total(self):
    rtn = []
    for j in self.x_stock.find("Total").itertext():
        if len(j.strip()) != 0:
            rtn.append(j)
    data_frame = {}
    for i in self.read_from_file():
        with open(i[0] + ".csv") as csv_file:
            content_list = csv.reader(csv_file, delimiter=',')
            for j in content_list:
                print(len(j))
                for k in j:
                    pattern = re.compile("([0-9]{4}-(([0][1-9])|([1][0-2]))-(([0][1-9])|([1-2][0-9])|([3][0-1])))+$")
                    if pattern.match(k):
                        j.remove(k)
                        tmp = {k: j}
                        if k in data_frame:
                            #print(len(data_frame[k]))
                            #print(len(tmp[k]))
                            #print(len(j))
                            for p in range(0, len(data_frame[k])):
                                data_frame[k][p] = data_frame[k][p] + j[p]
                        else:
                            data_frame.update(tmp)
                           #print(len(data_frame[k]))

    df = DataFrame(dict([(k, Series(v)) for k, v in data_frame.items()]))
    return [rtn, df]


def return_api(self):
    self.x_Info = self.x_doc.find("Information")
    return self.x_Info.find("API").find("API").text


def write_csv(self, data, stock_info):
    # opening the csv file in 'w' mode
    filename = stock_info[0] + ".csv"
    data_str = stock_info[3]
    file = open(filename, 'a+')

    for i in data:
        data_str = data_str + "," + str(i*float(stock_info[1]))

    with file:
        file.write(data_str + "\n")
