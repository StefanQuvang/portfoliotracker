class FileHandler:
    from ._Basic import add_to_basic, basic
    from ._Get import return_api, read_from_file, read_total, write_csv
    from ._Set import write_to_file, remove_from_file, construct_file, write_total

    def __init__(self, basic):
        self.File = False
        self.read_from_file()
        self.BasicInfo = basic
        if not self.File:
            self.construct_file()
