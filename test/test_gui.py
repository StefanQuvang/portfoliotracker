import unittest
from GUI import GUI


class TestGui(unittest.TestCase):
    def setUp(self):
        self.b = GUI()

    def test_is_digit_upper(self):
        self.assertTrue(self.b.is_digit("1"))
        self.assertTrue(self.b.is_digit(1))


    def test_is_digit_equal(self):
        self.assertTrue(self.b.is_digit("0"))
        self.assertTrue(self.b.is_digit(0))

    def test_is_digit_lower(self):
        self.assertTrue(self.b.is_digit("-1"))
        self.assertTrue(self.b.is_digit(-1))

    def test_is_digit_float_upper(self):
        self.assertTrue(self.b.is_digit("0.2"))
        self.assertTrue(self.b.is_digit(0.2))

    def test_is_digit_float_equal(self):
        self.assertTrue(self.b.is_digit("0.0"))
        self.assertTrue(self.b.is_digit(0.0))

    def test_is_digit_float_lower(self):
        self.assertTrue(self.b.is_digit("-0.1"))
        self.assertTrue(self.b.is_digit(-0.1))

    def test_is_digit_text(self):
        self.assertFalse(self.b.is_digit("H"))
        self.assertFalse(self.b.is_digit("2H"))
