import unittest
from Data import Data


class TestData(unittest.TestCase):
    def setUp(self):
        self.d = Data()

    def test_CalcReturn_div10(self):
        self.assertFalse(self.d.calc_return([1, 0], [0, 0]))

    def test_CalcReturn_div11(self):
        self.assertEqual(self.d.calc_return([1, 0], [1, 0]), 100)

    def test_CalcReturn_div01(self):
        self.assertEqual(self.d.calc_return([0, 1], [1, 1]), 50)

    def test_CalcReturn_div00(self):
        self.assertEqual(self.d.calc_return([0, 0], [1, 1]), 0)

    def test_CalcReturn_int01(self):
        self.assertEqual(self.d.calc_return(0, 1), 0)

    def test_CalcReturn_int10(self):
        self.assertFalse(self.d.calc_return(1, 0))

    def test_CalcReturn_int11(self):
        self.assertEqual(self.d.calc_return(1, 1), 100)

    def mockWriteTotal(self,standard_row, total):
        return True

    def test_getTotal(self):
        #Arrange
        self.d.reset()
        self.d.clFile.write_total = self.mockWriteTotal
        self.d.StockPrice.append(1000)
        self.d.AllStockReturns.append(1000)
        self.d.TotalReturn.append(1000)
        self.d.TotalStart.append(1000)
        # Act
        Result = self.d.get_total()
        # Assert
        self.assertEqual(Result, ["Total", 1000, "Daily Gain", 100, 1000, "Overall Gain", 100, 1000])

    def mockReadTotal(self):
        return ["Total", 1000, "Daily Gain", 1, 1000, "Overall Gain", 1, 1000]

    def test_getTotalFromFile(self):
        # Arrange
        self.d.reset()
        self.d.clFile.read_total = self.mockReadTotal
        # Act
        result = self.d.get_total()
        # Assert
        self.assertEqual(result, ["Total", 1000, "Daily Gain", 1, 1000, "Overall Gain", 1, 1000])

    def test_RemoveStock(self):
        # Arrange
        self.d.reset()
        self.d.StockList.append("test")
        self.d.StockPrice.append(10)
        self.d.AllStockReturns.append(10)
        self.d.TotalReturn.append(10)
        self.d.TotalStart.append(10)
        # Act
        self.d.remove_stock("test")
        # Assert
        self.assertEqual(len(self.d.StockList), 0)

    def MockGetStockInfo(self, stock):
        return [1*int(stock[1]), 1*int(stock[1]), 1*int(stock[1]), 1*int(stock[1]), 1*int(stock[1]), 1*int(stock[1]),
                1*int(stock[1]), 1*int(stock[1]), 1*int(stock[1]), 1*int(stock[1])]

    def mockWriteFile(self,name, content):
        return True

    def test_AddStock(self):
        # Arrange
        self.d.reset()
        self.d.clStock.get_stock_info = self.MockGetStockInfo
        self.d.clFile.write_to_file = self.mockWriteFile
        stock = ["Test", 2, 3]
        # Act
        stockinfo = self.d.add_stock(stock)
        #Assert
        self.assertEqual(stockinfo, [2, 2, 2, 2, 2, 2, 2, 2, 2, 2])
        self.assertEqual(self.d.StockList, ["Test"])
        self.assertEqual(self.d.StockPrice, [2])
        self.assertEqual(self.d.AllStockReturns, [2])
        self.assertEqual(self.d.TotalReturn, [2])
        self.assertEqual(self.d.TotalStart, [6])

    def test_AddStock_MultiStock(self):
        # Arrange
        self.d.reset()
        self.d.clStock.get_stock_info = self.MockGetStockInfo
        self.d.clFile.write_to_file = self.mockWriteFile
        stock1 = ["Stock 1", 2, 3]
        stock2 = ["Stock 2", 3, 4]
        # Act
        stockinfo1 = self.d.add_stock(stock1)
        stockinfo2 = self.d.add_stock(stock2)
        # Assert
        self.assertEqual(stockinfo1, [2, 2, 2, 2, 2, 2, 2, 2, 2, 2])
        self.assertEqual(stockinfo2, [3, 3, 3, 3, 3, 3, 3, 3, 3, 3])
        self.assertEqual(self.d.StockList, ["Stock 1", "Stock 2"])
        self.assertEqual(self.d.StockPrice, [2, 3])
        self.assertEqual(self.d.AllStockReturns, [2, 3])
        self.assertEqual(self.d.TotalReturn, [2, 3])
        self.assertEqual(self.d.TotalStart, [6, 12])

    def mockReadFromFile(self):
        stock_list = [
            ["orsted.co", "1000", "Daily Gain", 1, 1000, "Overall Gain", 1, 1000, 10, 10],
            ["test", 500, "Daily Gain", 1, 1000, "Overall Gain", 1, 1000, 10, 10],
            ["dummy", "250", "Daily Gain", 1, 1000, "Overall Gain", 1, 1000, 10, 10],
            ["dont Buy", 125, "Daily Gain", 1, 1000, "Overall Gain", 1, 1000, 10, 10]]
        return stock_list

    def test_get_stock(self):
        # Arrange
        self.d.clFile.read_from_file = self.mockReadFromFile
        # Act
        stocks = self.d.get_stock()
        # Assert
        self.assertEqual(self.d.StockList, ["orsted.co", "test", "dummy", "dont Buy"])
        self.assertEqual(self.d.StockPrice, [1000, 500, 250, 125])
        self.assertEqual(stocks, [
                                ["orsted.co", "1000", "Daily Gain", 1, 1000, "Overall Gain", 1, 1000, 10, 10],
                                ["test", 500, "Daily Gain", 1, 1000, "Overall Gain", 1, 1000, 10, 10],
                                ["dummy", "250", "Daily Gain", 1, 1000, "Overall Gain", 1, 1000, 10, 10],
                                ["dont Buy", 125, "Daily Gain", 1, 1000, "Overall Gain", 1, 1000, 10, 10]])

    def test_get_stock_again(self):
        # Arrange
        self.d.reset()
        self.d.StockList.append("orsted.co")
        self.d.StockPrice.append(1000)
        self.d.clFile.read_from_file = self.mockReadFromFile
        # Act
        stocks = self.d.get_stock()
        # Assert
        self.assertEqual(self.d.StockList, ["orsted.co", "test", "dummy", "dont Buy"])
        self.assertEqual(self.d.StockPrice, [1000, 500, 250, 125])
        self.assertEqual(stocks, [
                                ["orsted.co", "1000", "Daily Gain", 1, 1000, "Overall Gain", 1, 1000, 10, 10],
                                ["test", 500, "Daily Gain", 1, 1000, "Overall Gain", 1, 1000, 10, 10],
                                ["dummy", "250", "Daily Gain", 1, 1000, "Overall Gain", 1, 1000, 10, 10],
                                ["dont Buy", 125, "Daily Gain", 1, 1000, "Overall Gain", 1, 1000, 10, 10]])

    def test_return_rate(self):
        # Arrange
        self.d.reset()
        self.d.RateStr = "min"
        # Act
        str = self.d.return_rate()
        # Assert
        self.assertEqual(str, "min")

    def test_update_rate(self):
        # Arrange
        self.d.reset()
        rate_str = "sec"
        rate = 1
        # Act
        self.d.update_rate(rate_str)
        # Assert
        self.assertEqual(self.d.RateStr, rate_str)
        self.assertEqual(self.d.Rate, rate)

    def test_update_rate(self):
        # Arrange
        self.d.reset()
        rate_str = "min"
        rate = 60
        # Act
        self.d.update_rate(rate_str)
        # Assert
        self.assertEqual(self.d.RateStr, rate_str)
        self.assertEqual(self.d.Rate, rate)

    def test_update_rate(self):
        # Arrange
        self.d.reset()
        rate_str = "hour"
        rate = 60*60
        # Act
        self.d.update_rate(rate_str)
        # Assert
        self.assertEqual(self.d.RateStr, rate_str)
        self.assertEqual(self.d.Rate, rate)

    def test_update_rate(self):
        # Arrange
        self.d.reset()
        rate_str = "day"
        rate = 60*60*24
        # Act
        self.d.update_rate(rate_str)
        # Assert
        self.assertEqual(self.d.RateStr, rate_str)
        self.assertEqual(self.d.Rate, rate)

    def test_update_rate(self):
        # Arrange
        self.d.reset()
        rate_str = "default"
        default_rate = "min"
        rate = 60
        # Act
        self.d.update_rate(rate_str)
        # Assert
        self.assertEqual(self.d.RateStr, default_rate)
        self.assertEqual(self.d.Rate, rate)

    def test_update_stock(self):
        # Arrange
        self.d.reset()
        self.d.clStock.get_stock_info = self.MockGetStockInfo
        stock = [2, 2, 2, 2, 2, 2, 2, 2, 2, 2]
        # Act
        new_stock = self.d.update_stock(stock)
        # Assert
        self.assertEqual(new_stock, stock)

    def test_update_stock(self):
        # Arrange
        self.d.reset()
        self.d.clStock.get_stock_info = self.MockGetStockInfo
        self.d.ListOfStocks = [[8]]
        stock = [2, 2, 2, 2, 2, 2, 2, 2, 2, 2]
        # Act
        new_stock = self.d.update_stock(stock)
        # Assert
        self.assertEqual(new_stock, stock)

    def MockGetStockInfo2(self, stock):
        return [stock[0], 2, 2, 2, 2, 2, 2, 2, 2, 2]

    def test_update_stock(self):
        # Arrange
        self.d.reset()
        self.d.clStock.get_stock_info = self.MockGetStockInfo2
        stock = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        self.d.ListOfStocks.append(stock)
        self.d.StockPrice.append(1)
        self.d.AllStockReturns.append(1)
        self.d.TotalReturn.append(1)
        self.d.TotalStart.append(1)
        # Act
        new_stock = self.d.update_stock(stock)
        # Assert
        self.assertEqual(new_stock, [1, 2, 2, 2, 2, 2, 2, 2, 2, 2])
        self.assertEqual(self.d.ListOfStocks, [[1, 2, 2, 2, 2, 2, 2, 2, 2, 2]])
        self.assertEqual(self.d.StockPrice, [2])
        self.assertEqual(self.d.AllStockReturns, [2])
        self.assertEqual(self.d.TotalReturn, [2])
        self.assertEqual(self.d.TotalStart, [4])

    def test_add_total_value(self):
        # Arrange
        self.d.reset()
        self.d.StockPrice.append(2)
        self.d.StockPrice.append(4)
        # Act
        self.d.add_total_value()
        # Assert
        self.assertEqual(self.d.TotalValue, [6])
