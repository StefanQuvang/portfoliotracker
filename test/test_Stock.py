import unittest
from Stock import Stock


class TestStock(unittest.TestCase):
    def setUp(self):
        self.s = Stock()

    def test_credentials(self):
        # Arrange
        key = "ThisKey"
        # Act
        self.s.credentials(key=key)
        # Assert
        self.assertEqual(self.s.key, key)

    def test_SetApi(self):
        # Arrange
        api = "YahooFinance"
        # Act
        self.s.set_api(api)
        # Assert
        self.assertTrue(self.s.clAPI)

    def test_SetApi(self):
        # Arrange
        api = "NoAPI"
        # Act
        self.s.set_api(api)
        # Assert
        self.assertEqual(self.s.ApiText, "YahooFinance")

    def MockReturnRow(self):
        rtn = ['previousClose', 'ask']
        return rtn

    def MockGetInfo(self, tag_name, stock):
        if tag_name == "previousClose":
            return 10
        elif tag_name == 'ask':
            return 11

    def test_GetStockInfo(self):
        # Arrange
        self.s.clAPI.return_row = self.MockReturnRow
        self.s.clAPI.get_info = self.MockGetInfo
        stock = ["Stock1", 10, 10]
        # Act
        stockinfo = self.s.get_stock_info(stock=stock)
        # Assert
        self.assertEqual(stockinfo, ["Stock1", 100, "Daily Gain", (11/10-1)*100, 10, "Overall Gain", (11/10-1)*100, 10, 10, 10])
