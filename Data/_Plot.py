from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure
import matplotlib.pyplot as plt
from pandas import DataFrame


def add_chart(self, frame):
    if self.pc:
        self.pc.clear()

    fig = Figure(figsize=(4, 3), dpi=100, facecolor="black")  # create a figure object
    self.pc = fig.add_subplot(111)  # add an Axes to the figure

    self.pc.pie(self.StockPrice, radius=1, labels=self.StockList, autopct='%0.2f%%', shadow=True)
    fig.legend()
    chart1 = FigureCanvasTkAgg(fig, frame)
    return chart1.get_tk_widget()


def add_graph(self, frame):
    if self.gc:
        self.gc.clear()
    x_axis = []
    for i in range(0, len(self.TotalValue)):
        x_axis.append(i)

    data1 = {'Minute': x_axis,
             'Value': self.TotalValue
             }
    df1 = DataFrame(data1, columns=['Minute', 'Value'])

    figure1 = plt.Figure(figsize=(8, 4), dpi=100)
    ax1 = figure1.add_subplot(111)
    bar1 = FigureCanvasTkAgg(figure1, frame)
    df1 = df1[['Minute', 'Value']].groupby('Minute').sum()
    df1.plot(kind='line', legend=True, ax=ax1)
    ax1.set_title('Minute Vs. Value')

    return bar1.get_tk_widget()
