def get_total(self):
    if not self.TotalStart:
        total = self.clFile.read_total()[0]
    else:
        total = ["Total"]
        total.append(sum(self.StockPrice))
        total.append("Daily Gain")
        total.append(self.calc_return(rtn_val=self.AllStockReturns, price=self.StockPrice))
        total.append(round(sum(self.AllStockReturns), 2))
        total.append("Overall Gain")
        total.append(self.calc_return(rtn_val=self.TotalReturn, price=self.TotalStart))
        total.append(round(sum(self.TotalReturn), 2))
        self.clFile.write_total(standard_row=self.StandardRow, total=total)
    return total


def add_total_value(self):
        self.TotalValue.append(sum(self.StockPrice))


def calc_return(self, rtn_val, price):
    rtn = []
    pr = []
    try:
        len(rtn_val)
        rtn = rtn_val
        pr = price
    except TypeError:
        rtn.append(rtn_val)
        pr.append(price)
    try:
        return round(sum(rtn) / sum(pr) * 100, 2)
    except ZeroDivisionError:
        return 0
