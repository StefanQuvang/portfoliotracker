from File import FileHandler
from Stock import Stock
from datetime import date
import copy


def create_cl(self):
    self.clStock = Stock()
    today = date.today()
    basic = ["Name", "Stefan", "Time", today]
    self.clFile = FileHandler(basic)


def get_stock(self):
    stocks = self.clFile.read_from_file()
    tmp = []
    for i in self.StockList:
        for j in stocks:
            if i == j[0]:
                stocks.remove(j)
    for i in stocks:
        self.StockList.append(i[0])
        self.StockPrice.append(float(i[1]))
        self.AllStockReturns.append(float(i[4]))
        self.TotalReturn.append(float(i[7]))
        self.TotalStart.append(float(float(i[8])*float(i[9])))
        tmp.append(i[0])
        tmp.append(i[8])
        tmp.append(i[9])
        tmp.append(i[10])
        self.AddStocks.append(tmp)
        tmp = []
    if not self.ListOfStocks:
        self.ListOfStocks = self.clFile.read_from_file()
    return copy.deepcopy(self.ListOfStocks)


def add_stock(self, stock):
    for i in self.ListOfStocks:
        if i[0] == stock[0]:
            print("Stock already in portfolio")
            return []
    self.AddStocks.append(stock)
    self.StockList.append(stock[0])
    stock_info = self.clStock.get_stock_info(stock)[0]
    data = self.clStock.get_stock_info(stock)[1]
    stock.append(stock_info[10])
    self.AddStocks.append(stock)
    self.clFile.write_csv(data=data, stock_info=stock)
    self.StockPrice.append(stock_info[1])
    self.AllStockReturns.append(stock_info[4])
    self.TotalReturn.append(stock_info[7])
    self.TotalStart.append(float(stock[1])*float(stock[2]))
    self.clFile.write_to_file(name=self.StandardRow, content=stock_info)
    self.ListOfStocks.append(stock_info)
    return stock_info


def remove_stock(self, ticker):
    count = 0
    for stock in self.StockList:
        if stock == ticker:
            break
        else:
            count = count + 1
    for stock in self.ListOfStocks:
        if stock[0] == ticker:
            self.ListOfStocks.remove(stock)
    for stock in self.AddStocks:
        if stock[0] == ticker:
            self.AddStocks.remove(stock)
    del self.StockPrice[count]
    del self.AllStockReturns[count]
    del self.TotalReturn[count]
    del self.TotalStart[count]
    self.StockList.remove(ticker)
    self.clFile.remove_from_file(ticker)


def update_stock(self, stock):
    stock_info = self.clStock.get_stock_info(stock)[0]
    data = self.clStock.get_stock_info(stock)[1]
    self.clFile.write_csv(data=data, stock_info=stock)
    for stock_iter in range(0, len(self.ListOfStocks)):
        if self.ListOfStocks[stock_iter][0] == stock_info[0]:
            for stock_element in range(0, len(self.ListOfStocks[stock_iter])):
                self.ListOfStocks[stock_iter][stock_element] = stock_info[stock_element]
            self.StockPrice[stock_iter] = stock_info[1]
            self.AllStockReturns[stock_iter] = stock_info[4]
            self.TotalReturn[stock_iter] = stock_info[7]
            self.TotalStart[stock_iter] = float(stock_info[8])*float(stock_info[9])
            self.AddStocks[stock_iter][3] = stock_info[10]
    # Update File
    return stock_info


def update_rate(self, rate):
    self.RateStr = rate
    if rate == "sec":
        self.Rate = 1
    elif rate == "min":
        self.Rate = 60
    elif rate == "hour":
        self.Rate = 360
    elif rate == "day":
        self.Rate = 360*24
    else:
        self.RateStr = "min"
        self.Rate = 60


def return_rate(self):
    return self.RateStr
