class Data:

    instance = None

    def __init__(self):
        if not Data.instance:
            Data.instance = Data.__Data()

    def __getattr__(self, name):
        return getattr(self.instance, name)

    class __Data:
        from ._Plot import add_graph, add_chart
        from ._Total import calc_return, get_total, add_total_value
        from ._Stock import add_stock, remove_stock, create_cl, get_stock, update_rate, return_rate, update_stock

        def __init__(self):
            self.StockList = []
            self.ListOfStocks = []
            self.Rate = 60
            self.RateStr = "min"
            self.pc = None
            self.gc = None
            self.AddStocks = []
            self.TotalStockPrice = []
            self.StockPrice = []
            self.TotalReturn = []
            self.TotalStart = []
            self.AllStockReturns = []
            self.StandardRow = ["Ticker", "Total", "DailyGain", "Return", "ReturnDay", "OverallGain", "Return",
                                "TotalReturn", "Number", "BuyPrice", "BuyDate"]
            self.TotalValue = []
            self.create_cl()

        # This method is only for unit testing of singleton.
        def reset(self):
            self.StockList = []
            self.ListOfStocks = []
            self.AddStocks = []
            self.TotalStockPrice = []
            self.StockPrice = []
            self.TotalReturn = []
            self.TotalStart = []
            self.AllStockReturns = []
